#include <stdio.h>

#define TABN 7

int main()
{
	int c, stub, ost;
	
	ost = TABN;
	stub = 0;
	while ((c = getchar()) && c != EOF) {
		if (c == '\t') {
			ost = TABN - stub;
			for (int i = 0; i < ost+1; ++i) {
				++stub;
				putchar(' ');
			}			
		} else {
			if (c != '\n') {
				++stub;
			}
			putchar(c);
		}
		
		if (stub > TABN || c == '\n') {
			stub = 0;
		}
	}
}
