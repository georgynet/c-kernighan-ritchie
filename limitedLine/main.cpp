#include <stdio.h>

#define MAXLEN 10

int getBuffer(char[], int lengthString);

int main()
{
	int simbol, spacePosition, length = MAXLEN;
	char buffer[MAXLEN];
	
	while ((spacePosition = getBuffer(buffer, length)) || spacePosition == 0) {
		
		int i = 0;
		length = MAXLEN;
		
		while (buffer[i] != '\0') {
			putchar(buffer[i]);
			if (i == spacePosition) {
				putchar('\n');
			}
			++i;
			--length;
		}
		
		if (spacePosition == -1 && buffer[i] == '\0') {
			putchar('\n');
		}
		
		if (length == 1 || buffer[i-1] == '\n') {
			length = MAXLEN;
		}
	}
	
	return 0;
}

int getBuffer(char buffer[], int lengthString)
{
	int simbol, i, spacePosition = -1;
	
	for (i = 0; i < lengthString-1 && (simbol = getchar()) != EOF && simbol != '\n'; ++i) {
		if (simbol == ' ' && simbol != EOF) {
			spacePosition = i;
		}
        buffer[i] = simbol;
	}
	
	if (simbol == '\n') {
		spacePosition = -1;
	}
	
	if (simbol == '\n') {
        buffer[i] = simbol;
        ++i;
    }
    
    buffer[i] = '\0';
	
	return spacePosition;
}
